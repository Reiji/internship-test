using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private float speed = 5f;
        [SerializeField] private MouseLook mouseLook;

        private Transform cameraTransform;
        private CharacterController characterController;
        private CollisionFlags collisionFlags;
        
        private void Start()
        {
            characterController = GetComponent<CharacterController>();
            cameraTransform = Camera.main.transform;
			mouseLook.Init(transform , cameraTransform);
        }

        private void Update()
        {
            RotateView();
        }

        private void FixedUpdate()
        {
            Vector2 input = GetInput();
            Vector3 move = transform.forward * input.y + transform.right * input.x;

            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo, characterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);

            move = Vector3.ProjectOnPlane(move, hitInfo.normal).normalized * (speed * Time.fixedDeltaTime);

            if (!characterController.isGrounded)
                move += Physics.gravity * Time.fixedDeltaTime;

            collisionFlags = characterController.Move(move);

            mouseLook.UpdateCursorLock();
        }


        private Vector2 GetInput()
        {
            Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            if (input.sqrMagnitude > 1)
                input.Normalize();

            return input;
        }


        private void RotateView()
        {
            mouseLook.LookRotation (transform, cameraTransform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;

            if (collisionFlags == CollisionFlags.Below || body == null || body.isKinematic)
                return;

            body.AddForceAtPosition(characterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
    }
}
